module painter.core.color;

//TODO: 	добавить возможность кастовать из одного типа цветов
//			в другой без изменения этой структуры. Потом удалить 
//			из структуры opCast'ы и модули ниже

//import 	dlib.image;

//version(Windows) {
//	import 	dfl.all;
//}

struct RGBColor
{
	ubyte R, G, B;
	
	this(ubyte R, ubyte G, ubyte B)
	{
		this.R = R;
		this.G = G;
		this.B = B;
	}
	
	/*version(Windows) {
	// перегружаем преобразование нашей структуры в аналогичную из DFL
            Color opCast(T : Color)()
            {
                    return Color(R,G,B);
            }  
        }*/
}