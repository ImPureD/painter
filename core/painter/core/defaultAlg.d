module painter.core.defaultAlg;

import painter.core;

// стандартный алгоритм отрисовки
class DefaultLine(/*S, C*/) : surfaceLine {
private:
	//S surface;
	//C color;
	RGBColor 		color;
	//ColorCasting!C 	castClass;

	surfacePoint 	pointInterface;
	
	// рисование линии методом DDA, т.к стандартный примитив отсутствует
	void drawDDALine(/*ref S simg, C color, */int x1, int y1, int x2, int y2)
	{
		import std.algorithm : max;
		import std.math : abs;
		
		int deltaX = abs(x1 - x2);
		int deltaY = abs(y1 - y2);
		int length = max(deltaX, deltaY);
		
		//pointInterface.setColor(castClass.TtoRGB(color));
		pointInterface.setColor(color);
		
		if (length == 0) {
			pointInterface.draw(x1, y1);
		}
		
		float dx = cast(float) (x2 - x1) / cast(float) length;
		float dy = cast(float) (y2 - y1) / cast(float) length;
		float x = x1;
		float y = y1;
		
		length++;
		while(length--)
		{
			x += dx;
			y += dy;

			pointInterface.draw(cast(int) x, cast(int) y);
		}
	}
	
public:
	// Точка знает, куда нужно рисовать
	this(/*S surface,*/ surfacePoint pointInterface/*, ColorCasting!C castClass*/)
	{
		//this.surface = surface;
		this.pointInterface = pointInterface;
		//this.castClass = castClass;
	}
	
	// Да и цвет примитиву в принципе можно не давать, можно передать рисующей точке прямо в Surface. Надо подумать над этим
	void setColor(RGBColor color)
	{
		//this.color = castClass.RGBtoT(color);
		this.color = color;
	}
	
	void draw(int[] points...)
	{
		auto firstX = points[0];
		auto firstY = points[1];
		
		for (int i = 2; i < points.length; i += 2)
		{
			drawDDALine(/*surface, color, */firstX, firstY, points[i], points[i+1]);
			firstX = points[i];
			firstY = points[i+1];
		}
	}
}