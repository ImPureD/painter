module painter.core.interfaces;

import painter.core.color;

// Интерфейс должен быть, чтобы разработчики могли определить кастинг из типа в тип
interface ColorCasting(C) {
	C RGBtoT (RGBColor);
	RGBColor TtoRGB (C);
}

// интерфейс для точки
// возможно, будет использоваться в качестве карандаша
interface surfacePoint {
	// void setColor(ubyte R, ubyte G, ubyte B); // как вариант
	void setColor(RGBColor color);
	void draw(int X, int Y);
}

// интерфейс для обычной ломанной (прямая - частный случай для ломанной)
interface surfaceLine {
	void setColor(RGBColor color);
	void draw(int[] points...);
}

// интерфейс для треугольника
interface surfaceTriangle {
    void setColor(RGBColor color);
	void draw(int x1, int y1, int x2, int y2, int x3, int y3, bool filled = false);
}

// интерфейс для прямоугольника
interface surfaceRectangle {
    void setColor(RGBColor color);
	void draw(int x1, int y1, int width, int height, bool filled = false);
}

// интерфейс для параллелограмма
//Стоит ли объединить паралелограмм и прямоугольник???
interface surfaceParallelogram {
    void setColor(RGBColor color);
	void draw(int x1, int y1, int width, int height, bool filled = false);
}

//интерфейс для круга
interface surfaceCircle {
	void setColor(RGBColor color);
	void draw(int x, int y, int radius, bool filled = false);
}