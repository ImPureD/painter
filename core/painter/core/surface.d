module painter.core.surface;

import painter.core;

abstract class Surface
{
protected:
	surfaceLine line;
	surfacePoint point;
	RGBColor color;
	
public:
	void setColor(RGBColor color)
	{
		this.color = color;
	}
	
	// делегируем отрисовку конкретному экземпляру, реализующему интерфейс
	void drawPoint(int X, int Y)
	{
		point.setColor(color);
		point.draw(X, Y);
	}
	
	// делегируем отрисовку конкретному экземпляру, реализующему интерфейс
	void drawLine(int[] points...)
	{
		line.setColor(color);
		line.draw(points);
	}
}