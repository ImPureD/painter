﻿module dflBinding.point;

import dflBinding; // импортируем весь модуль

// обертка над точкой DFL2
class dflPoint : surfacePoint
{
private: 
	Graphics graphics;
	Color color;
	
public:
	this(Graphics graphics)
	{
		this.graphics = graphics;
	}
	
	void setColor(RGBColor color)
	{
		this.color = cast(Color) color;
	}
	
	void draw(int X, int Y)
	{
		Pen pen = new Pen(color, 1);
		graphics.drawLine(pen, X, Y, X + 1, Y + 1);
	}
}