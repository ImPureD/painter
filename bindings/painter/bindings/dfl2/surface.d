﻿module dflBinding.surface;

import dflBinding; // импортируем весь модуль

// поверхность DFL2
class dflSurface : Surface
{
	this(Graphics graphics)
	{
		point = new dflPoint(graphics);
		line =  new dflLine(graphics);
	}
}