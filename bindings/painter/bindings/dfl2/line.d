﻿module dflBinding.line;

import dflBinding; // импортируем весь модуль

// обертка над линией DFL
class dflLine : surfaceLine
{
private:
	Graphics graphics;
	Color color;
	
public:
	this(Graphics graphics, Color color = Color(0,0,0))
	{
		this.graphics = graphics;
	}
	
	void setColor(RGBColor color)
	{
		this.color = cast(Color) color;
	}
	
	void draw(int[] points...)
	{
		Point[] XY;
		Pen pen = new Pen(color, 1);
		
		for (int i = 0; i < points.length; i += 2)
		{
			XY ~= Point(points[i], points[i+1]);
		}
		
		graphics.drawLines(pen, XY);
	}
}
