﻿module painter.bindings.dlib; // привязки для dlib

public:
	import painter.core;
	import dlib.image;

	import painter.bindings.dlib.point;
 	import painter.bindings.dlib.line;
 	import painter.bindings.dlib.surface;
 	import painter.bindings.dlib.colorCasting;