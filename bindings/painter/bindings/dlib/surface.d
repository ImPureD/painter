﻿module painter.bindings.dlib.surface;

import painter.bindings.dlib; // импортируем весь модуль

class dlibSurface : Surface
{
	this(SuperImage superImage)
	{
		point = new dlibPoint(superImage);
		line =  new dlibLine(point);
	}
}