﻿module painter.bindings.dlib.line;

import painter.bindings.dlib;  // импортируем весь модуль

// линия в dlib
alias dlibLine = DefaultLine!();
//class dlibLine : surfaceLine  
//{
//private:
//	SuperImage superImage;
//	Color4f color;
	
//	// Алгоритм DDA-линии для рисования линии в dlib
//	// алгоритм лучше заменить - дает очень жесткую пикселизацию
//	void drawDDALine(ref SuperImage simg, Color4f color, int x1, int y1, int x2, int y2)
//	{
//		import std.algorithm : max;
//		import std.math : abs;
		
//		int deltaX = abs(x1 - x2);
//		int deltaY = abs(y1 - y2);
//		int length = max(deltaX, deltaY);
		
//		if (length == 0)
//		{
//			simg[x1, y1] = color;
//		}
		
//		float dx = cast(float) (x2 - x1) / cast(float) length;
//		float dy = cast(float) (y2 - y1) / cast(float) length;
//		float x = x1;
//		float y = y1;
		
//		length++;
//		while(length--)
//		{
//			x += dx;
//			y += dy;
//			simg[cast(int) x, cast(int) y] = color;
//		}
		
//	}
	
//public:
//	this(SuperImage superImage)
//	{
//		this.superImage = superImage;
//	}
	
//	void setColor(RGBColor color)
//	{
//		this.color = cast(Color4f) color;
//	}
	
//	void draw(int[] points...)
//	{
//		auto firstX = points[0];
//		auto firstY = points[1];
		
//		for (int i = 2; i < points.length; i += 2)
//		{
//			drawDDALine(superImage, color, firstX, firstY, points[i], points[i+1]);
//			firstX = points[i];
//			firstY = points[i+1];
//		}
//	}
//}