﻿module painter.bindings.dlib.point;

import painter.bindings.dlib;  // импортируем весь модуль

// точка в dlib
class dlibPoint : surfacePoint
{
private:
	SuperImage superImage;
	Color4f color;
	
public:
	this(SuperImage superImage)
	{
		this.superImage = superImage;
	}
	
	void setColor(RGBColor color)
	{
		this.color = (new dlibColorCasting).RGBtoT(color);
	}
	
	void draw(int X, int Y)
	{
		superImage[X, Y] = color;
	}
}