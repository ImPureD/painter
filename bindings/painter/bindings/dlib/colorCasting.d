module painter.bindings.dlib.colorCasting;

import painter.bindings.dlib;

static class dlibColorCasting : ColorCasting!(Color4f) {
	override  Color4f RGBtoT (RGBColor color) {
		float r = color.R / 255.0f;
		float g = color.G / 255.0f;
		float b = color.B / 255.0f;

		return Color4f(r, g, b);
	}

	override RGBColor TtoRGB (Color4f color) {
		ubyte R = cast(ubyte)(color.r * 255);
		ubyte G = cast(ubyte)(color.g * 255);
		ubyte B = cast(ubyte)(color.b * 255);

		return RGBColor(R, G, B);
	}
}